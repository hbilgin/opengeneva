import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
         <app-app-header></app-app-header>
         <router-outlet></router-outlet>`
})
export class AppComponent {
  title = 'swiss';
}
