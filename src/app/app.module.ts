import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppHeaderModule } from './common/components/app-header/app-header.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule } from './common/core/auth/auth.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SnackBarComponent } from './common/components/snack-bar/snack-bar.component';

import { AgmCoreModule } from '@agm/core';
import '@angular/compiler';

@NgModule({
  declarations: [
    AppComponent,
    SnackBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppHeaderModule,
    BrowserAnimationsModule,
    AuthModule,
    FlexLayoutModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAvcDy5ZYc2ujCS6TTtI3RYX5QmuoV8Ffw',
      libraries: ['places']
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
