import { JsonProperty } from 'json-object-mapper';

export class PublicPlace {

  @JsonProperty()
  id: number = void 0;

  @JsonProperty()
  name: string = void 0;

  @JsonProperty()
  description: string = void 0;

  @JsonProperty()
  type: string = void 0;

  @JsonProperty()
  free: boolean = void 0;

  @JsonProperty()
  latitude: number = void 0;

  @JsonProperty()
  longitude: number = void 0;

}

