import { JsonProperty, JsonIgnore } from 'json-object-mapper';
import { PublicPlace } from './public-place';

export class City{

  @JsonProperty()
  id: number = void 0;

  @JsonProperty()
  name: string = void 0;

  @JsonProperty()
  description: string = void 0;

  @JsonProperty()
  population: number = void 0;

  @JsonProperty()
  latitude: number = void 0;

  @JsonProperty()
  longitude: number = void 0;

  @JsonProperty()
  publicPlaces: number[] = void 0;

  @JsonIgnore()
  places: PublicPlace[] = void 0;

}
