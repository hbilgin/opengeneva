import { JsonProperty, JsonIgnore } from 'json-object-mapper';

export class UserProfile {

  @JsonProperty()
  firstName: number = void 0;

  @JsonProperty()
  lastName: string = void 0;

  @JsonProperty()
  login: string = void 0;

  @JsonIgnore()
  token: string = void 0;

}
