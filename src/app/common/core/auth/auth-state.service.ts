import { Injectable } from '@angular/core';
import { UserProfile } from './models/user-profile';
import * as  CryptoJs from 'crypto-js';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthStateService {

  constructor() { }

  getToken(): string{
    let token = null;
    const encrypted = sessionStorage.getItem('token');
    if(encrypted) {
      token = CryptoJs.AES
              .decrypt(encrypted, environment.secretKey)
              .toString(CryptoJs.enc.Utf8);
    }
    return token;
  }

  setToken(token: string){
    if(token) {
      sessionStorage.setItem('token', CryptoJs.AES.encrypt(token, environment.secretKey));
    }
  }

  setUser(userProfile: UserProfile){
    sessionStorage.setItem('userProfile', JSON.stringify(userProfile));
  }

  getUser(): UserProfile{
    try {
      return JSON.parse(sessionStorage.getItem('userProfile'), );
    } catch (e) {
      return null;
    }
  }

  logOut() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
  }

  isLogged() {
    const token  = sessionStorage.getItem('token');
    return token != null ;
  }
}
