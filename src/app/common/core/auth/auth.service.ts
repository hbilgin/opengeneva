import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { UserProfile } from './models/user-profile';
import { environment } from 'src/environments/environment';
import { ObjectMapper } from 'json-object-mapper';
import { AuthStateService } from './auth-state.service';
import { Router } from '@angular/router';

export interface Credential {
  login: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }

  constructor(private httpClient: HttpClient,
              private authState: AuthStateService,
              private router: Router) { }

    authenticate(credentiel: Credential): Observable<UserProfile> {
      return this.httpClient
      .post<UserProfile>(environment.apiUrl + '/security/authenticate',
         credentiel,
        {headers: this.headers,observe: 'response'})
      .pipe(
          tap(res => this.authState.setToken(res.headers.get('Authorization'))),
          map(res => res.body),
          map(user => ObjectMapper.deserialize(UserProfile, user)),
          map((user:UserProfile) => {
            this.authState.setUser(user);
            return user;
          })
        )
    }

    logOut() {
      //TODO rest logout
      this.authState.logOut();
      this.router.navigateByUrl('/login');
    }

}
