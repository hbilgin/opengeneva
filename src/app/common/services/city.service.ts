import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { City } from '../models';
import { environment } from 'src/environments/environment';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ObjectMapper } from 'json-object-mapper';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
  }
  constructor(private http: HttpClient) { }

  cities(): Observable<City[]> {
    return this.http
      .get<City[]>(environment.mockApiUrl + '/cities', {headers: this.headers})
      .pipe(
        map((cities: Object[]) => ObjectMapper.deserializeArray(City, cities))
      );
  }

  city(cityId: number): Observable<City>{
    return this.http.get<City>(environment.mockApiUrl + '/cities/' +cityId)
      .pipe(
        map((city: Object) => ObjectMapper.deserialize(City, city))
      );
  }

  addCity(city: City): Observable<City> {
    return this.http
      .post<City>(environment.mockApiUrl + '/cities',city)
      .pipe(
        map((city: Object) => ObjectMapper.deserialize(City, city))
      );
  }

  editCity(city: City): Observable<City> {
    return this.http
      .put<City>(environment.mockApiUrl + '/cities/' +city.id,city)
      .pipe(
        map((city: Object) => ObjectMapper.deserialize(City, city))
      );
  }

  removeCity(cityId: number) : Observable<void> {
    return this.http
    .delete<void>(environment.mockApiUrl + '/cities/' + cityId);
  }
}
