import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CityService } from 'src/app/common/services';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { City } from 'src/app/common/models';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-city',
  templateUrl: './edit-city.component.html',
  styleUrls: ['./edit-city.component.scss']
})
export class EditCityComponent implements OnInit {
  cityForm: FormGroup;
  city: City;

  constructor(private fb: FormBuilder,
              private cityService: CityService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    console.log('edit');
    this.activatedRoute.paramMap.pipe(
      switchMap((params: ParamMap) => {

        return this.cityService.city(parseInt(params.get('id')));
      })
    ).subscribe(res => this.initForm(res));
  }

  initForm(city: City) {
    this.city = city;
    this.cityForm = this.fb.group({
      name:[city.name, Validators.required],
      description:[city.description, Validators.required],
      latitude:[city.latitude, Validators.required],
      longitude:[city.longitude, Validators.required],
    });
  }

  //--------HANDLERS------------

  editCity(){
    const city: City = Object.assign({}, this.cityForm.value);
    city.id = this.city.id;
    this.cityService.editCity(city)
      .subscribe(
        res => this.router.navigateByUrl('/'),
        err => this._snackBar.open('err', err.message)
      )
  }
}
