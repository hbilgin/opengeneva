import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditCityComponent } from './edit-city.component';

const routes: Routes = [
  { path :'',   component: EditCityComponent },
  { path :'**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditCityRoutingModule {
}
