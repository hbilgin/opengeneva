import { Component, OnInit } from '@angular/core';
import { CityService } from 'src/app/common/services/city.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-city-edit',
  templateUrl: './create-city.component.html',
  styleUrls: ['./create-city.component.scss']
})
export class CreateCityComponent implements OnInit {
  cityForm: FormGroup;

  constructor(private cityService: CityService,
              private fb: FormBuilder,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.cityForm = this.fb.group({
      id: this.fb.control(null, [Validators.required, Validators.min(0)]),
      name: ['', Validators.required],
      description: ['', Validators.required],
      latitude: [null, Validators.required],
      longitude: [null, Validators.required],
      publicPlaces: this.fb.array([
        this.placeGenerator()
      ])
    });
  }

  get publicPlaces(){
    return this.cityForm.get('publicPlaces') as FormArray;
  }

  //-------HANDLERS--------------

  addPlace(){
    this.publicPlaces.push(this.placeGenerator());
  }

  addCity(){
    this.cityService.addCity(this.cityForm.value)
      .subscribe(
        res => this._snackBar.open('City added'),
        err => this._snackBar.open('error', err.message)
      );
  }

  // -------HELPERS--------------

  placeGenerator() {
    return this.fb.group({
      id: this.fb.control(null, [Validators.required, Validators.min(0)]),
      name: this.fb.control('', [Validators.required]),
    });
  }
}
