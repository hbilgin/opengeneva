import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateCityComponent } from './create-city.component';

const routes: Routes= [
  { path: '', component: CreateCityComponent}
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateCityRoutingModule {

}
