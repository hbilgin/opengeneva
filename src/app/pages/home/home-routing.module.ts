import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { AuthGuard } from 'src/app/common/core/auth/auth.guard';
import { AuthModule } from 'src/app/common/core/auth/auth.module';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: HomeComponent},
  {
    path: 'new',
    canActivate: [AuthGuard],
    loadChildren: () => import('./create-city/create-city.module')
                            .then(m => m.CreateCityModule)},
  {
    path: 'edit/:id',
    canActivate: [AuthGuard],
    loadChildren: () => import('./edit-city/edit-city.module')
                              .then(m => m.EditCityModule)
  },
  {path: '/**', pathMatch: 'full', redirectTo: ''}
];

@NgModule({
  imports: [
            RouterModule.forChild(routes),
            AuthModule
            ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
