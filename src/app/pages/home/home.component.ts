import { Component, OnInit } from '@angular/core';
import { City } from 'src/app/common/models';
import { CityService } from 'src/app/common/services/city.service';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  cities: City[];
  selectedCity: City;

  constructor(private cityService: CityService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(){
    console.log('cities');
    this.cityService.cities()
      .subscribe(
        res => this.cities =res,
        err => this._snackBar.open('error', err.message)
      );
  }

  selectCity(city: City) {
    this.selectedCity = city;
  }

  removeCity(cityId: number) {
    console.log('removecity :', cityId);
    this.cityService.removeCity(cityId)
      .subscribe(
        res => {
          this._snackBar.open('City deleted', '', {
          duration: 3000
          });
          this.ngOnInit();
        },
        err => this._snackBar.open('error', err.message)
      );
    }

}
