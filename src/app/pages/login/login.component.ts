import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/common/core/auth/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthStateService, UserProfile } from 'src/app/common/core/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login = '';
  password = '';

  constructor(private authService: AuthService,
              private authState: AuthStateService,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  getUser() {
    const user: UserProfile = this.authState.getUser();
    return user.login;
  }

  userLoggedIn(){
    return this.authState.isLogged();
  }

  // ----------HANDLERS-----------

  loginUser() {
    const credential = {
      login: this.login,
      password: this.password
    };
    this.authService.authenticate(credential)
      .subscribe(
        res => this.router.navigate(['/']),
        err => {
          let message = 'Error, please retry!';
          if (err.message) {
            message = err.message;
          }
          this.snackBar.open('Error',message, {
            duration: 3000
          });
          console.error('login error', err);
        }
      );
  }

  logOut() {
    this.authService.logOut();
  }

}
